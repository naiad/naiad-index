#-*- coding: utf-8 -*-
"""
A tool to extract the coverage and metadata from EO data, that can be later
registered into a indexing engine such as Elasticsearch.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from setuptools import setup, find_packages

setup(
    zip_safe=False,
    name='naiad-index',
    version='0.1.0',
    author='Jeff Piolle <jfpiolle@ifremer.fr>',
    author_email='jfpiolle@ifremer.fr',
    packages=find_packages(),
    entry_points={
        'naiad_index.tilers': [
            'spherical = naiad_index.processing.spherical:SphericalTiler',
            'geohash = naiad_index.processing.geohash:GeohashTiler',
            'grid = naiad_index.processing.geohash:GridTiler',
        ],
        'naiad_index.property_plugins': [
            'GHRSSTTiler = naiad_index.processing.contribs.ghrssttiler:GHRSSTTiler',
            'IASIEPSTiler = naiad_index.processing.contribs.iasiepstiler:IASIEPSTiler',
            'S3OLCITiler = naiad_index.processing.contribs.s3olcitiler:S3OLCITiler',
            'S3SLSTRTiler = naiad_index.processing.contribs.s3slstrtiler:S3SLSTRTiler',
        ]
    },
    scripts=['bin/naiad-tile',
             'bin/naiad-inspect-file',
    ],
    license='LICENSE.txt',
    description='A tool to extract the coverage and metadata from EO data',
    long_description=open('README.rst').read(),
    install_requires=[
        'xarray>0.15',
        'shapely',
        'astropy',
        'qd',
        'spherical_geometry',
        'pyyaml',
        'elasticsearch',
        'pyinterp'
    ],
)
