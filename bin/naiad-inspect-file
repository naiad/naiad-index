#!/usr/bin/env python
"""
Display the content of a naiad tile file, created with `naiad-index`.


:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import argparse
import logging

import matplotlib.pyplot as plt

from naiad.data.tile import Tile


def get_options():
    parser = argparse.ArgumentParser(
        description='Display the content of a granule tile file')

    parser.add_argument(
        'naiad_tile_file', type=str,
        help='path to the naiad tile file')
    parser.add_argument(
        '-f', '--footprint-only', action='store_true',
        help='display only the global footprint without the tiles')
    parser.add_argument(
        '-t', '--tiles-only', action='store_true',
        help='display only the tiles')
    return parser.parse_args()


if __name__ == '__main__':
    args = get_options()

    footprint, tiles = Tile.read_tiles(
        args.naiad_tile_file, granule_only=args.footprint_only)

    # show result
    fig = None
    if not args.tiles_only:
        fig = Tile.draw(footprint.shape, colour="red")

    if not args.footprint_only:
        for tile in tiles:
            if fig is None:
                fig = Tile.draw(tile.shape, colour="green")
            else:
                Tile.draw(tile.shape, colour="green", ax=fig)

    plt.show()


