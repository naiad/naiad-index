#!/usr/bin/env python
"""
Command line tool for tiling Earth Observation data granules

:copyright: Copyright 2015 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""

import argparse
import logging
import datetime
import pkg_resources
from pathlib import Path
import typing as T
import sys

from pydantic import BaseModel, Extra

import naiad_commons.data
import naiad_index.utils as utils
import naiad_index.cli as cli

from naiad_index.processing.baseindexer import BaseIndexerSettings


LOGGER_LEVEL = logging.INFO
LOGGER_FORMAT_STRING = '| %(asctime)s | %(levelname)-8s| %(message)-100s'


class IndexerCommand(BaseModel):

    class Config:
        extra = Extra.ignore
        arbitrary_types_allowed = True

    # verbosity
    verbosity: utils.VerbosityConfig = utils.VerbosityConfig()

    indexer: BaseIndexerSettings = BaseIndexerSettings()

    # input file to process
    input: Path

    # execution args
    fail_silently: bool = False

    # display and control arguments
    # display interactively the created global footprint and tiles
    show: bool = False

    # display only the global footprint without the tiles
    show_footprint_only: bool = False


def add_general_args(parser):
    parser.add_argument(
        '--fail-silently', action='store_true',
        help='do not raise exception when some sanity checks fail'
    )

    # show arguments
    show_group = parser.add_mutually_exclusive_group()
    show_group.add_argument(
        '--show', action='store_true',
        help='display interactively the created global footprint and tiles'
    )
    show_group.add_argument(
        '--show-footprint-only', action='store_true',
        help='display only the global footprint without the tiles'
    )
    parser.add_argument(
        'input', type=str, help='input file path (or pattern)'
    )

    subparsers = parser.add_subparsers(
        title='indexing plugins', dest='indexer_plugin')

    # arguments for the available types of tilers
    for entry_point in pkg_resources.iter_entry_points('naiad_index.indexer'):
        indexer = entry_point.load()
        parser_indexer = subparsers.add_parser(
            indexer.identifier, help=indexer.help)
        indexer.add_indexing_args(parser_indexer)

    return parser.parse_args()


def decode_indexer_args(args):
    vargs = vars(args)
    dargs = IndexerCommand(**vargs)

    dargs.verbosity = utils.VerbosityConfig(**vargs)

    indexer_class = {
        entry_point.name: entry_point
        for entry_point
        in pkg_resources.iter_entry_points('naiad_index.indexer')
    }[args.indexer_plugin].load()
    dargs.indexer = indexer_class.setting_class(**vargs)

    return dargs, indexer_class


def parse_args(cli_args):
    parser = argparse.ArgumentParser(description='Tile EO data granule files')

    # general arguments
    utils.add_verbosity_args(parser)
    add_general_args(parser)

    return decode_indexer_args(parser.parse_args(cli_args))


def main(cli_args=None):
    args, tiler_class = parse_args(cli_args)

    # Set up verbosity option.
    cli.set_verbosity(args.verbosity.level)
    
    start = datetime.datetime.now()

    logging.basicConfig(format=LOGGER_FORMAT_STRING)
    logger = logging.getLogger()
    logger.setLevel(LOGGER_LEVEL)

    # processor for tiling
    tiler = tiler_class()

    logging.debug("Run tiling")
    tile_files = tiler.create_index(
        args.input,
        args.indexer,
        fail_silently=not args.fail_silently
    )

    # # show result
    if args.show:
        for f in tile_files:
            georecord = naiad_commons.data.geoindexrecord.read_csv(f)
            georecord.show(show_segments=not args.show_footprint_only)

    end = datetime.datetime.now()
    logging.debug("Elapsed time : %s" % (end - start))

    if len(tile_files) == 0:
        logging.info('No saved output file as there were no tiles found for '
                     'these file(s)')
        sys.exit(2)
    else:
        for f in tile_files:
            logging.info('Saved: {}'.format(f))
        sys.exit(0)
