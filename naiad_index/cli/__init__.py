from enum import Enum
import logging


logger = logging.getLogger()
logger.setLevel(logging.WARNING)


class VerbosityEnum(str, Enum):
    verbose = 'verbose'
    quiet = 'quiet'
    silent = 'silent'


def logging_level(verbosity: VerbosityEnum):
    try:
        return {
            VerbosityEnum.verbose: logging.DEBUG,
            VerbosityEnum.quiet: logging.WARNING,
            VerbosityEnum.silent: logging.FATAL
        }[verbosity]
    except KeyError:
        return logging.INFO


def set_verbosity(verbosity: VerbosityEnum):
    """Set the verbosity"""
    logger.setLevel(logging_level(verbosity))

