import naiad_index.processing.geohashindexer as gh
import naiad_index.processing.segmentindexer as tiler


def geohashes(feature, **kwargs):
    """Returns the geohashes matching the feature"""
    return gh.to_geohash(feature, **kwargs)


def footprint(feature, method='spherical', **kwargs):
    """
    Extract the spatial footprint of a feature as a Shapely geometry.

    Methods:
        geohash: based on the geohash codes of the feature data positions.
            Slower but more fitted to sparse or holed data, less prone to
            invalid polygons.
        spherical: based on spherical geometry polygons or lines. Adapted to
            continous, gap free features (no invalid lat/lon, no holes or gaps).
    """
    if method == 'geohash':
        return gh.footprint(feature, **kwargs)
    elif method == 'spherical':
        return tiler.footprint(feature, **kwargs)
    else:
        ValueError("Unknown footprint extraction method: {}".format(method))
