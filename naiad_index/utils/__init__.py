import logging
from pathlib import Path

from pydantic import BaseModel

from naiad_index.cli import VerbosityEnum


class VerbosityConfig(BaseModel):
    level: VerbosityEnum = 'verbose'
    fail_silently: bool = False
    benchmark: bool = False


def add_verbosity_args(parser):
    # Add verbosity control.
    verbosity_group = parser.add_mutually_exclusive_group()
    verbosity_group.add_argument(
        "-v", "--verbose", action="store_true",
        help="Activate debug level logging - for extra feedback."
    )
    verbosity_group.add_argument(
        "-q", "--quiet", action="store_true",
        help="Disable information logging - for reduced feedback."
    )
    verbosity_group.add_argument(
        "-s", "--silent", action="store_true",
        help="Log ONLY the most critical or fatal errors."
    )
    parser.add_argument(
        '--benchmark', action='store_true',
        help='provides the time elasped between start and end of execution'
        )
    return verbosity_group
