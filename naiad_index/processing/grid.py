"""
A list of functions to support geohash based indexing and geometries

Note on precision: use the precision (geohash length) immediately greater than
the resolution of the data (precision 5 for 1km resolution data, 4 for 20 km
data,...)

Geohash length 	Cell width 	Cell height
1 	≤ 5,000km 	× 	5,000km
2 	≤ 1,250km 	× 	625km
3 	≤ 156km 	× 	156km
4 	≤ 39.1km 	× 	19.5km
5 	≤ 4.89km 	× 	4.89km
6 	≤ 1.22km 	× 	0.61km
7 	≤ 153m 	× 	153m
8 	≤ 38.2m 	× 	19.1m
9 	≤ 4.77m 	× 	4.77m
10 	≤ 1.19m 	× 	0.596m
11 	≤ 149mm 	× 	149mm
12 	≤ 37.2mm 	× 	18.6mm


"""
import logging
from typing import List

import geopandas as gpd
from polygon_geohasher.polygon_geohasher import \
    polygon_to_geohashes, geohashes_to_polygon
import matplotlib.pyplot as plt
import numpy as np
from shapely import geometry, ops

from naiad.data.tile import Tile
from .segmentindexer import Tiler


# number of level+1 geohashes for a geohash of a given precision level
GH_COUNT = 32

TILING_DEFAULTS = {
    'dataset_class': None,
    'feature_class': None,
    'tiler': 'geohash',
    'x_step': None,
    'y_step': None,
    'dilate': None,
    'simplify': None,
    'resolution': None,
    'orientation': None,
    'view': None,
    'archive': False,
    'tile_output': '.',
    'fill_missing': False,
    'granule_only': False,
    'precision': 4,
    'mask_variable': None,
    'fit_to_valid_area': False,
}


def latlon_to_geohash(
        lat, lon, precision=4, mask: 'np.ndarray' = None) -> List[str]:
    """Returns the list of geohashes for a list of positions"""
    try:
        import pyinterp.geohash as geohash
    except ModuleNotFoundError:
        logging.error(
            'geohash package is not installed. Optional geohash based tiling '
            'is not available. Install geohash package.')
        raise

    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)

    clat = np.ma.masked_where(lat >= 90., lat).compressed().tolist()
    clon = np.ma.masked_where(lat >= 90., lon).compressed().tolist()
    geo = np.unique(geohash.encode(clon, clat, precision=precision))

    return geo.astype(np.unicode_)


def to_geohash(feature,
               precision: int = 4,
               mask: 'np.ndarray' = None,
               **kwargs) -> List[str]:
    """Returns the list of geohashes for a cerbere feature"""
    lat = feature.get_lat(expand=True)
    lon = feature.get_lon(expand=True)
    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)
    return latlon_to_geohash(lat, lon, precision)


def latlon_to_footprint(lat, lon, precision: int = 4, **kwargs) -> List[str]:
    """Returns the footprint polygon matching a list of positions, based on
    geohash extraction method.

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.
    """
    geohashes = latlon_to_geohash(lat, lon, precision)
    reduced_geohashes = reduce(geohashes, **kwargs)

    return to_polygon(reduced_geohashes, **kwargs)


def footprint(feature,
              precision: int = 4,
              mask: 'np.ndarray' = None,
              **kwargs) -> List[str]:
    """Returns the footprint polygon matching a cerbere feature, based on
    geohash extraction method

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.

    Args:
        mask: mask of invalid measurements (to remove these locations from the
            footprint computation)
    """
    lat = feature.get_lat(expand=True)
    lon = feature.get_lon(expand=True)
    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)
    return latlon_to_footprint(lat, lon, precision=precision, **kwargs), []


def reduce(geohashes: List[str], min_level: int = 2, **kwargs) -> List[str]:
    """
    Reduce a list of geohashes so that level+1 geohashes are
    replaced with a level geohash whenever possible.

    Args:
        geohashes: list of geohashes of identical level
        min_level: minimum precision to which geohashes are reduced

    Return:
        a reduced list of geohashes of different precision
    """
    if len(geohashes) == 0:
        return geohashes

    geohashes.sort()

    reduced = []
    remainders = []
    k = 0
    while True:
        if k == len(geohashes):
            break
        if k + GH_COUNT >= len(geohashes):
            remainders.extend(geohashes[k:])
            break

        next_geo = set(
            [_[:-1] for _ in geohashes[k:min(k + GH_COUNT, len(geohashes))]])
        if len(next_geo) == 1:
            reduced.append(geohashes[k][:-1])
            k += GH_COUNT
        else:

            remainders.append(geohashes[k])
            k += 1

    if len(geohashes[0]) > min_level and len(reduced) > 0:
        reduced = reduce(reduced)

    reduced.extend(remainders)
    return reduced


def to_polygon(geohashes, convex=True, **kwargs):
    """
    Returns the polygon matching a set of geohashes.

    Contiguous geohashes are considered as belonging to the same polygon. Apply
    only on a reduced list of geohashes as it can be extremely inefficient on a
    large number of geohashes.

    Args:
        convex: simplify by taking the convex hull of the returned polygon to
            avoid aliasing effect. Uses with caution.
    """
    polygon = geohashes_to_polygon(geohashes)

    # simplify by taking the convex hull
    if convex:
        if isinstance(polygon, geometry.MultiPolygon):
            return ops.cascaded_union([_.convex_hull for _ in polygon])
        else:
            return polygon.convex_hull

    return polygon


def plot(feature=None, positions=None, geohashes=None, polygon=None, **kwargs):
    try:
        import pyinterp.geohash as geohash
    except ModuleNotFoundError:
        logging.error(
            'geohash package is not installed. Optional geohash based tiling '
            'is not available. Install geohash package.')
        raise
    fig, axs = plt.subplots(figsize=(10,10), **kwargs)
    if geohashes is not None:
        import shapely
        p = gpd.GeoSeries(
            [shapely.wkt.loads(
                str(geohash.bounding_box(_).wkt())) for _ in geohashes])
        gpd.GeoSeries.plot(p, ax=axs, aspect='auto', color='g', edgecolor='k')
    if feature is not None:
        plt.scatter(feature.get_lon(), feature.get_lat(), alpha=0.3, s=0.1)
    if positions is not None:
        lat, lon = positions
        plt.scatter(lon, lat, alpha=0.6, s=0.1)
    if polygon is not None:
        p = gpd.GeoSeries(polygon)
        gpd.GeoSeries.plot(
            p, ax=axs, aspect='auto', facecolor="none", edgecolor='r')

    plt.show()


class GeohashTiler(Tiler):

    def discrete_geometry(self, lons, lats, **kwargs):
        return Point(lons, lats)

    def segment_geometry(self, lons, lats, **kwargs):
        return GeoShape.polyline(lons, lats)

    def tile_from_polygon(
            self,
            lons, lats,
            start, end,
            feature,
            xmin, xmax,
            ymin, ymax,
            precision=4,
            mask: 'np.ndarray' = None,
            **kwargs):
        """return a Tile object from a feature subset

        Args:
            lons: longitude array of the data contained in the tile
            lats: latitude array of the data contained in the tile
            start: minimum sensing time of the data contained in the tile
            start: maximum sensing time of the data contained in the tile
            xmin: start offset of feature subset in X-axis
            xmax: stop offset of feature subset in X-axis
            ymin: start offset of feature subset in Y-axis
            ymax: stop offset of feature subset in Y-axis
            precision: geohash precision used to retrieve the shape of the
                data contained in the tile.
            mask: mask to apply to the data to fit the shape to valid data in
                the tile only. The mask must fit lats and lons dimensions.
        """
        geohashes = latlon_to_geohash(
            lats, lons, precision=precision, mask=mask)
        reduced_geohashes = reduce(geohashes, **kwargs)

        if False:
            # for debug
            plot(feature=None,
                 positions=(lats, lons),
                 geohashes=reduced_geohashes,
                 polygon=to_polygon(reduced_geohashes, **kwargs))

        return Tile(
            feature.url.name,
            to_polygon(reduced_geohashes, **kwargs),
            start,
            end,
            feature=feature.__class__.__name__,
            ymin=ymin,
            ymax=ymax,
            xmin=xmin,
            xmax=xmax,
            properties={'geohashes': reduced_geohashes}
        )

    def get_bounding_tile(self, feature, tiles):
        import itertools
        geohashes = itertools.chain.from_iterable(
            [tile.properties['geohashes'] for tile in tiles]
        )
        reduced_geohashes = reduce(list(set(geohashes)))

        tile = Tile(feature.url.name,
                    to_polygon(reduced_geohashes),
                    feature.time_coverage_start,
                    feature.time_coverage_end,
                    feature=feature.__class__.__name__,
                    )

        # remove geohashes from tiles
        for t in tiles:
            t.properties = {}

        return tile

