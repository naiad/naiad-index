"""
.. module::naiad.ingestion.tiler

Base class to extract the spatial, temporal and other properties from a
granule with the option of devide the granule into subsets or segments that
are indexed too. Segment indexing allows finer temporal selection and
smaller scale properties.


:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from abc import abstractmethod
import logging
import glob
import math
from pathlib import Path
import typing as T

try:
    import matplotlib.pyplot as plt
except:
    logging.warning(
        "matplotlib is missing. Using display functions will fail"
    )
import numpy as np
import shapely

import cerbere
from cerbere.feature.cbasefeature import BaseFeature
from cerbere.feature.ctrajectory import Trajectory
from cerbere.feature.cswath import Swath
from cerbere.feature.cgrid import Grid


from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord
from naiad_commons.data.geoshape import WORLD

from naiad_index.processing.baseindexer import BaseIndexer, \
    BaseIndexerSettings, NoFootprintMethod


class NoValidTimes(Exception):
    pass


class SegmentIndexerSettings(BaseIndexerSettings):

    # divides the granule into tiles or segments and extract metadata for each
    # subset too (extract only granule level metadata by default)
    index_segments: bool = False

    x_step: int = None
    y_step: int = None
    segment_steps: T.List[str] = None

    resolution: float = None
    orientation: str = 'counterclockwise'
    view: str = None

    fill_missing: bool = False
    mask_variable: str = None
    fit_to_valid_area: bool = False


class SegmentIndexer(BaseIndexer):
    """Class to extract the shape and tiles from a granule"""
    @classmethod
    def add_indexing_args(cls, parser):

        super(SegmentIndexer, cls).add_indexing_args(parser)

        # granule level arguments
        parser.add_argument(
            '--index-segments', action='store_true',
            help=(
                'divides the feature into joint subsets (segments) and extract '
                'metadata  for each segment too (extract only feature level '
                'metadata by default)')
        )
        parser.add_argument(
            '--segment-steps', nargs='*', type=str, dest='segment_steps',
            help='the dimensions and corresponding array offset along which '
                 'the feature should be segmented (ex: "time:10")'
        )
        parser.add_argument(
            '--tiling-method', type=str,
            choices=["corners", "convex_hull", "geohash"],
            help='method for subtiling'
        )
        parser.add_argument(
            '--resolution', type=float,
            help='resolution of the data, in degrees (optional, calculated'
                 ' from data by default'
        )
        parser.add_argument(
            '--orientation', type=str,
            default='counterclockwise',
            choices=['clockwise', 'counterclockwise'],
            help='orientation of swath pixels (clockwise or '
                 'counterclockwise. If not provided, the tiler '
                 'tries to guess from lat/lon.'
        )
        parser.add_argument(
            '--property-plugin', type=str,
            help='specify here the custom plugin to use to extract '
                 'the properties of the feature and its segments index '
                 'records. By default the indexer does not extract any '
                 'properties, only the temporal and spatial boundaries of the '
                 'feature and its segments'
        )
        parser.add_argument(
            '-m', '--mask-variable', type=str,
            help='variable to use as mask where no tiles should be produced'
        )
        parser.add_argument(
            '--fit-to-valid-area', action='store_true',
            help='fit the footprint or tile shapes to the unmasked data (if '
                 '--mask-variable is set). Will produce irregular geometries.'
        )

    @staticmethod
    def _decode_segment_steps(arg, field_geo):
        steps_per_dim = {_.split(':')[0]: int(_.split(':')[1]) for _ in arg}
        for k in field_geo.dims:
            if k not in steps_per_dim:
                raise ValueError(f'dimension {k} is missing in the '
                                 f'segmentation steps (--segment-steps)')
        return steps_per_dim

    @classmethod
    def set_segment_geometry(
            cls,
            georecord: SegmentIndexRecord,
            feature: BaseFeature,
            mask: np.ndarray = None,
            **kwargs):
        """set the geometry of a feature's FeatureIndexRecord object by joining the
        geometries of its segments.

        Args:
            mask: mask to apply to the data to fit the shape to the valid data
                only witin the segment. The mask must fit lat and lon
                dimensions.
        """
        raise NotImplementedError

    @classmethod
    def discrete_geometry(cls, lons, lats, **kwargs):
        raise NotImplementedError

    @classmethod
    def _segment_discrete(
            cls,
            feature: BaseFeature,
            lons,
            lats,
            times,
            **kwargs) -> SegmentIndexRecord:
        """Returns the segments for a discrete point collection feature

        Tiles with no valid time or lat/lon are not returned.

        Args:
            lons (array)
            lats (array)
            times (array)
            step : size of the tiles to extract
        """
        tiles = []
        
        for i in range(len(times)):
            
            if np.ma.is_masked(lons[i]):
                continue

            tile = SegmentIndexRecord(
                granule=feature.url.name,
                feature=feature.feature_type,
                geometry=cls.discrete_geometry(lons[i], lats[i]),
                start=times[i],
                end=times[i],
                slices={'time': slice(i, i+1)}
                )
            tiles.append(tile)

        return tiles

    def polyline_geometry(self, lons, lats, **kwargs):
        raise NotImplementedError

    def _segment_polyline(
            self,
            feature: BaseFeature,
            mask: str = None,
            segment_steps: str = None,
            gap_threshold: float = None,
            fit_to_valid_area: bool = False,
            **kwargs):
        """Returns the polygonal tiles for a one dimensional feature
        (ex: Trajectory).

        Tiles with no valid time or lat/lon are not returned.

        Args:
            lons (array)
            lats (array)
            times (array)
            steps : size of the segments to extract as tiles


        See also:
            Refer also :func:`naiad.processing.geoshape.make2d` and
            :func:`naiad.processing.geoshape.make2d` for additional optional
            arguments.
        """
        # get one-dimensional coordinates
        lats, lons, times = self._coordinates(feature)

        # locations to ignore in the segmentation
        mask = self._mask(feature, mask, times if fit_to_valid_area else None)

        # defines segment default sizes
        if segment_steps is None:
            steps_per_dim = {
                dim: size
                for dim, size in feature.ds.lat.sizes.items()}
        else:
            steps_per_dim = SegmentIndexer._decode_segment_steps(
                segment_steps, feature.ds.lat)

        if len(steps_per_dim) != 1:
            raise ValueError(f'Segmentation by polygon only accepts '
                             f'one-dimensional division')

        seg_size = list(steps_per_dim.values())[0]
        seg_dim = list(steps_per_dim.keys())[0]

        segments = []
        dim = len(lons)
         
        # calculate distance from one pixel to the next
        dists = np.ma.sqrt(
            np.ma.power((lons[:-1] - np.roll(lons, -1)[:-1]), 2) +
            np.ma.power((lats[:-1] - np.roll(lats, -1)[:-1]), 2)
            ).filled(0)
        if gap_threshold is not None:
            dists = np.ma.masked_greater_equal(dists, gap_threshold)
        else:
            dists = np.ma.array(dists, copy=False)

        istart = 0

        # find valid data segments within the required length
        while istart < dim - 1:
            
            # start at first valid indice
            if np.ma.is_masked(lons[istart:]):
                edges = np.ma.flatnotmasked_edges(lons[istart:])
                if edges is None:
                    return segments
                istart = istart + edges[0]

            iend = min(istart + seg_size, dim - 1)
            if iend != dim - 1:
                next = iend - 1
            else:
                next = iend
                            
            # end before first invalid indice
            if np.ma.is_masked(lons[istart:iend]):
                iend = istart + np.ma.flatnotmasked_contiguous(
                    lons[istart:iend]
                    )[0].stop
                next = iend
            
            # break segment at large interpixel distance
            if dists[istart:iend].count() == 0:
                istart = iend
                continue
            if np.ma.is_masked(dists[istart:iend]):
                iend = istart + np.ma.flatnotmasked_contiguous(
                    dists[istart:iend]
                    )[0].stop + 1
                next = iend

            offset = slice(istart, iend)
            start = times[offset].min().values
            end = times[offset].max().values

            # skip this tile if no valid time
            if start is None:
                istart = iend
                continue

            try:
                segment = SegmentIndexRecord(
                    granule=feature.ds.cb.url.name,
                    feature=feature.featureType,
                    geometry=self.polyline_geometry(lons[offset], lats[offset]),
                    start=start,
                    end=end,
                    slices={seg_dim: offset})
                segments.append(segment)
            except:
                print(self.polyline_geometry(lons[offset], lats[offset]))
                logging.debug("Skipped an invalid tile: {}".format(offset))
                raise
            
            istart = next

        return segments

    @staticmethod
    def equalize_step(dimsize: int, step: int):
        # if the dimsize is not a multiple of step size
        if math.modf(dimsize / float(step))[0] != 0:
            # adjust step size to get equal size segments
            # get smaller and higher steps that gives equal size and keep
            # the closest to the original step value
            ldivisor = max(math.floor(dimsize / float(step)), 1)
            lystep = math.ceil(dimsize / ldivisor)
            hdivisor = max(math.ceil(dimsize / float(step)), 1)
            hystep = math.ceil(dimsize / hdivisor)
            if abs(lystep - step) < abs(hystep - step):
                nstep = int(lystep)
            else:
                nstep = int(hystep)
            if nstep == 0:
                nstep = 1

            if nstep != step:
                logging.debug(f'step adjusted from {step} to {nstep} to '
                              f'equalize the segment sizes')
                step = nstep

        return step

    def _segment_polygon(
            self,
            feature,
            mask: str = None,
            segment_steps: str = None,
            orientation: str = 'counterclockwise',
            gap_threshold: float = None,
            equalize_segments: bool = True,
            fit_to_valid_area: bool = False,
            **kwargs):
        """Returns the polygonal segments for a two-dimensional feature
        (ex: Swath or Grid).

        Segments with no valid time or lat/lon are not returned.

        Args:
            lons (array)
            lats (array)
            times (array)
            x_step : size of the tiles along i-axis (cell for Swath or i/lon
                for Grid)
            y_step: size of the tiles along j-axis (row for Swath or j/lat for
                Grid)
            resolution (float): resolution of the data, in degrees
            orientation (str): native orientation of the input data
            gap_threshold (float): maximum tile size along y axis in cartesian
                coordinates
            mask: mask to apply to the data to fit the shape to valid data in
                the tile only. The mask must fit coordinate dimensions.
            fit_to_valid_area: ignore in the segmentation the measurements
                without valid coordinates
        """
        # get 2-dimensional coordinates
        lats, lons, times = self._coordinates(feature)

        # locations to ignore in the segmentation
        mask = self._mask(feature, mask, times if fit_to_valid_area else None)

        # defines segment default sizes
        if segment_steps is None:
            steps_per_dim = {
                dim: size
                for dim, size in feature.ds.cb.latitude.sizes.items()}
        else:
            steps_per_dim = SegmentIndexer._decode_segment_steps(
                segment_steps, feature.ds.cb.latitude)

        if len(steps_per_dim) != 2:
            raise ValueError(f'Segmentation by polygon only accepts '
                             f'two-dimensional division')

        dims = feature.ds.cb.latitude.dims
        sizes = feature.ds.cb.latitude.sizes
        y_step = steps_per_dim[dims[0]]
        x_step = steps_per_dim[dims[1]]
        if equalize_segments:
            y_step = SegmentIndexer.equalize_step(sizes[dims[0]], y_step)
            x_step = SegmentIndexer.equalize_step(sizes[dims[1]], x_step)

        # remove empty edge rows and columns in feature coordinates
        yedges = np.ma.notmasked_edges(lats, axis=0)
        y0 = yedges[0][0].min()
        yn = yedges[1][0].max() + 1

        xedges = np.ma.notmasked_edges(lats, axis=1)
        x0 = xedges[0][1].min()
        xn = xedges[1][1].max() + 1
        
        tiles = []
        j = y0
        while j < yn:

            # avoid having one remaining row and ensure a tile has at least
            # two rows to avoid tile shapes like lines : extend the current
            # tile to the end of the data
            jsup = min(j + y_step, yn)
            
            # test maximum size along y-axis
            if gap_threshold is not None:
                above = True
                while above:
                    sublats = lats[j:jsup, :]
                    minlat = sublats[np.ma.argmin(sublats, 0)]
                    maxlat = sublats[np.ma.argmax(lats[j:jsup, :], 0)]
                    if np.ma.fabs(maxlat - minlat).max() >= gap_threshold:
                        jsup -= 1
                    else:
                        above = False 

            if jsup == j:
                j += 1
                continue

            for i in range(x0, xn, x_step):

                if orientation == "counterclockwise":
                    xl, xr = (i, min(i + x_step, xn))
                else:
                    xl, xr = (min(i + x_step, xn), i)
                xleft = min(xl, xr)
                xright = max(xl, xr)

                ttimes = times[j:jsup, xleft:xright]

                # no valid times
                if np.isnat(ttimes).count() == 0:
                    logging.debug("Skipped an invalid segment (no valid times)")
                    continue

                start = ttimes.to_masked_array().min()
                end = ttimes.to_masked_array().max()

                tmask = None
                if mask is not None:
                    tmask = mask[j:jsup, xleft:xright]

                logging.debug("subset : %d %d %d %d" % (xleft, xright, j, jsup))
                segment = SegmentIndexRecord(
                    feature.ds.cb.url.name,
                    feature.feature_type,
                    start=start,
                    end=end,
                    slices=FeatureIndexRecord._slices_from_csv(
                        [j, jsup, xleft, xright], feature.feature_type))

                if segment is None:
                    logging.debug("Skipped an invalid tile: {}".format(
                        [xleft, xright, j, jsup]
                    ))
                else:
                    self.set_segment_geometry(segment, feature, mask, **kwargs)
                    if segment.geometry is not None:
                        tiles.append(segment)

            j = jsup

        logging.debug("All tiles processed")
        return tiles

    def _coordinates(self, feature):
        """Check coordinates validity and returns them as a tuple of (lat,
        lon, time).
        """
        lons = feature.ds.lon.cb.isel(broadcast=True)
        lats = feature.ds.lat.cb.isel(broadcast=True)
        times = feature.ds.time.cb.isel(broadcast=True)

        if lons.shape != lats.shape or lons.shape != times.shape:
            raise ValueError(
                "Inconsistent dimensions for lat/lon/time coords. You may be "
                "using the wrong cerbere Dataset class for this product.")

        if times.count() == 0:
            logging.error("No valid times in dataset.")
            raise NoValidTimes("No valid times in dataset.")

        return lats, lons, times

    def _mask(
            self,
            feature: BaseFeature,
            mask_variable: str = None,
            mask_array: np.ma.MaskedArray = None
    ) -> T.Union[np.ma.MaskedArray, None]:
        """Returns the mask to be applied to the coordinates. Masked
        coordinates won't be taken into account in the segmentation.

        Args:
            feature: the feature being indexed
            mask_variable: the variable in the feature to take the mask
              from, to ignore all measurements where this variable is masked
            mask_array: the masked array in the feature to take the mask
              from, to ignore all measurements where this array is masked
        """
        if mask_variable is None and mask_array is None:
            return

        masks = []
        if mask_variable is not None:
            masks.append(np.ma.getmaskarray(feature.get_values(mask_variable)))
        if mask_array is not None:
            masks.append(np.ma.getmaskarray(masks))

        if len(masks) == 1:
            return masks[0]

        return np.logical_or(masks[0], masks[1])

    def segment(
            self,
            feature: BaseFeature,
            settings: SegmentIndexerSettings) -> T.List[FeatureIndexRecord]:
        """Returns the segments within a feature, according to some
        division criteria.

        Segments with no valid time or lat/lon are not returned.

        Args:
            feature: the cerbere Feature object to segment
        """
        if isinstance(feature, Trajectory):
            segments = self._segment_polyline(
                feature, **settings.dict())

        elif isinstance(feature, (Swath, Grid)):
            segments = self._segment_polygon(
                feature, **settings.dict())

        elif feature.featureType.lower() in ['omdcollection']:
            segments = self._segment_discrete(
                feature, **settings.dict())

        else:
            raise ValueError(
                "feature must be either a Swath, Grid, OMDCollection, or "
                "a Trajectory")

        return segments

    @classmethod
    def process_tile_properties(cls, feature, tiles):
        """Add the additional properties to store in the product index for a
        tile.
        """
        # method to be implemented in derived classes
        return

    @abstractmethod
    def bounding_geometry(
            self,
            segments: T.List[SegmentIndexRecord],
            feature_type: str,
            simplify_factor: float = 0.1,
            **kwargs) -> shapely.geometry.base:
        """return the bounding geometry of a FeatureIndexRecord object from
        the jointure of its segments coverages.

        Args:
            segments: the list of segments part of the feature
            feature_type: the type of feature as defined in cerbere
            simplify_factor: simplify (as in shapely) factor applied to the
                joint geometry
        """
        raise NotImplementedError

    def _geoindexrecord_from_feature(
            self,
            feature: BaseFeature,
            settings: SegmentIndexerSettings,
            skip_feature_geometry: bool = False
    ) -> FeatureIndexRecord:
        """Extract the index metadata of the footprint and segments of a
        feature

        Args:
            skip_feature_geometry: skip the calculation of the feature
               footprint from its segments (geometry attribute is left to None).
        """
        georecord = FeatureIndexRecord(
                feature.ds.cb.url.name,
                feature.featureType,
                start=feature.ds.cb.time_coverage_start,
                end=feature.ds.cb.time_coverage_end,
            )

        # segment the data if segments index are to be returned or if the
        # bounding geometry of the feature needs to be calculated from the
        # segments within the feature
        if settings.index_segments or not skip_feature_geometry:
            # extract segment
            segments = self.segment(feature, settings)
            if len(segments) == 0:
                raise ValueError("Did not find any valid segment.")

            if settings.index_segments:
                georecord.segments = segments

            if not skip_feature_geometry:
                logging.info('The feature geometry will be estimated from its '
                             'segmentation.')
                georecord.geometry = self.bounding_geometry(
                    segments, feature.featureType
                )
                logging.debug("Bounding shape : %s", georecord.geometry)

        return georecord

    def finalize(
            cls,
            feature: BaseFeature,
            georecord: FeatureIndexRecord,
            dilate: float = None,
            simplify: float = None,
            **kwargs):
        """Process additional properties and adjust the retrieved spatial
        coverage.
        """
        # apply granule level finalize
        super(SegmentIndexer, cls).finalize(
            feature, georecord, dilate, simplify, **kwargs
        )

        if georecord.segments is None:
            return

        # complement segment metadata with custom properties
        if len(georecord.segments) > 0:
            cls.process_tile_properties(feature, georecord.segments)

        # dilate segments if required
        if dilate is not None:
            for segment in georecord.segments:
                segment.geometry = segment.geometry.buffer(
                    dilate, cap_style=3, resolution=8
                )

        # simplify segments if required
        if simplify is not None:
            for segment in georecord.segments:
                segment.geometry = segment.geometry.simplify(
                    simplify, preserve_topology=True)

        # ensure geometry is within world boundaries
        for segment in georecord.segments:
            segment.geometry = segment.geometry.intersection(WORLD)

    def geoindexrecord_from_feature(
            self,
            feature: BaseFeature,
            settings: SegmentIndexerSettings) -> FeatureIndexRecord:
        """Extract an index record from a feature. If required, extract
        segments index records too.

        Args:
            feature: feature object to extract an index record from
            settings: extraction settings
        """
        # get feature boundaries from arguments or global attributes if asked
        try:
            georecord = super(SegmentIndexer, self).geoindexrecord_from_feature(
                feature, settings)

            if settings.index_segments:
                boundaries = georecord.geometry
                # get segments from feature segmentation
                georecord: FeatureIndexRecord = \
                    self._geoindexrecord_from_feature(
                        feature, settings, skip_feature_geometry=True)
                georecord.geometry = boundaries

        # not asked => get feature boundaries and segments from feature
        # segmentation
        except NoFootprintMethod:
            georecord: FeatureIndexRecord = self._geoindexrecord_from_feature(
                feature, settings)

        return georecord

    def save(self, georecord: FeatureIndexRecord, outfile):
        outfile.write(georecord.encode_csv())
        if georecord.segments is None:
            return
        for seg in georecord.segments:
            outfile.write(seg.encode_csv())
