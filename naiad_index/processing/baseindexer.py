"""
Base class to extract the spatial, temporal and other properties from a granule

:copyright: Copyright 2021 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import logging
import glob
from pathlib import Path
import typing as T

try:
    import matplotlib.pyplot as plt
except:
    logging.warning(
        "matplotlib is missing. Using display functions will fail"
    )
from pydantic import BaseModel, Extra
import shapely

import cerbere
from cerbere.feature.feature import Feature

from naiad_commons.data.geoindexrecord import FeatureIndexRecord
from naiad_commons.data.geoshape import WORLD


class NoFootprintMethod(Exception):
    pass


class BaseIndexerSettings(BaseModel):

    class Config:
        extra = Extra.ignore
        arbitrary_types_allowed = True

    # format and feature type for loading the data
    dataset_class: T.Optional[str] = 'xarray'
    feature_class: str = 'Swath'

    # extract the spatial coverage from the bounding box in global CF attributes
    # rather than the lat/lon
    footprint_from_attrs: bool = False
    # extract the file spatial coverage from the provided WKT string
    footprint_from_wkt: str = None

    indexer: str = 'basic'

    dilate: float = None
    simplify: float = None

    # organize as year/day the output index files
    archive: bool = False
    output_dir: Path = Path()


class BaseIndexer(object):
    """Base class to extract the spatial, temporal and other properties
    from a granule"""

    identifier: str = 'basic'
    help: str = 'use basic spatial information extraction. No tiling possible.'
    setting_class = BaseIndexerSettings

    @classmethod
    def add_indexing_args(cls, parser):

        parser.add_argument(
            '--dataset-class', type=str, default='xarray',
            help='Cerbere dataset class to read the input data'
        )
        parser.add_argument(
            '--feature-class', type=str, dest='feature_class',
            choices=['Swath', 'Grid', 'CylindricalGrid', 'Trajectory'],
            default='Swath',
            help='Cerbere feature class for the input data.'
        )

        parser.add_argument(
            '--footprint-from-attrs', action='store_true',
            help='extract the file spatial coverage from the bounding box in '
                 'global CF attributes rather than the lat/lon'
        )
        parser.add_argument(
            '--footprint-from-wkt', type=str,
            help='extract the file spatial coverage from the provided WKT string'
        )

        parser.add_argument(
            '--dilate', type=float,
            help='dilatation factor of the spatial geometries (as in shapely)'
        )
        parser.add_argument(
            '--simplify', type=float,
            help='simplification factor the spatial geometries (as in shapely)'
        )

        parser.add_argument(
            '--output-dir', type=str,
            help='output repository for the index files'
        )
        parser.add_argument(
            '--archive', action='store_true',
            help='store index files in year/day subfolders'
        )

    @classmethod
    def process_granule_properties(cls, feature, record):
        """Add the additional properties to store in the product index for a
        granule.
        """
        # method to be implemented in derived classes
        return

    def finalize(
            self,
            feature: Feature,
            georecord: FeatureIndexRecord,
            dilate: float = None,
            simplify: float = None,
            **kwargs):
        """Process additional properties and adjust the retrieved spatial
        coverage.
        """
        # complement with custom properties
        self.process_granule_properties(feature, georecord)

        # dilate footprint if required
        if dilate is not None:
            georecord.geometry = georecord.geometry.buffer(
                dilate,
                cap_style='square',
                resolution=8,
                join_style=shapely.geometry.JOIN_STYLE.mitre
            )

        # simplify footprint if required
        if simplify is not None:
            georecord.geometry = georecord.geometry.simplify(
                simplify, preserve_topology=True)

        # ensure geometry is within world boundaries
        georecord.geometry = georecord.geometry.intersection(WORLD)

    def geoindexrecord_from_feature(
            self,
            feature: Feature,
            settings: BaseIndexerSettings) -> FeatureIndexRecord:
        """Extract an index record from a feature"""
        if not settings.footprint_from_attrs and \
                not settings.footprint_from_wkt:
            raise NoFootprintMethod(
                'no valid methode provided to extract the feature spatial '
                'coverage')

        if settings.footprint_from_attrs:
            geometry = feature.bbox
        else:
            geometry = shapely.wkt.loads(settings.footprint_from_wkt)

        return FeatureIndexRecord(
            feature.url.name,
            feature.__class__.__name__,
            geometry=geometry,
            start=feature.time_coverage_start,
            end=feature.time_coverage_end,
            segments=[]
        )

    @classmethod
    def display(
            cls,
            georecord: FeatureIndexRecord,
            footprint_only: bool = False):
        # show result
        fig = FeatureIndexRecord.draw(georecord.geometry, colour="red")
        plt.show()

    def save(self, georecord: FeatureIndexRecord, outfile):
        outfile.write(georecord.encode_csv())

    def create_index(
            self,
            pattern: T.Union[str, Path],
            settings: BaseIndexerSettings,
            **kwargs) -> T.List[Path]:
        """Creates the index files from the input file(s)"""
        files = glob.glob(str(pattern))
        if len(files) == 0:
            logging.warning(
                "No files were found with this pattern: {}".format(pattern)
            )
        files = [Path(_) for _ in files]
        files.sort()

        outfiles = []
        for i, fname in enumerate(files):
            logging.info("Processing #{} : {}".format(str(i), fname))
            logging.debug("Open file")
            feature = cerbere.open_feature(
                fname, reader=settings.dataset_class
            )

            # extract index georecord
            georecord = self.geoindexrecord_from_feature(feature, settings)

            # add custom properties and adjust geometry
            self.finalize(feature, georecord, **settings.dict())

            # save
            outfname = georecord.to_csv(settings.output_dir, settings.archive)
            outfiles.append(outfname)

            logging.info('Saved: {}'.format(outfname))

            feature.ds.close()

        return outfiles
