import logging
import typing as T

import numpy as np
from shapely.geometry import LinearRing, MultiLineString, LineString, Point
import shapely.ops

from cerbere.feature.feature import Feature

from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord
from naiad_commons.data.geoshape import GeoShape, WORLD_WEST0, WORLD

from naiad_index.processing.segmentindexer import SegmentIndexer, SegmentIndexerSettings


class SphericalIndexerSettings(SegmentIndexerSettings):
    gap_threshold: float = None


class SphericalIndexer(SegmentIndexer):

    identifier: str = 'spherical'
    help: str = 'use spherical geometry based spatial information extraction'
    setting_class = SphericalIndexerSettings

    @classmethod
    def add_indexing_args(cls, parser):
        """Adding specific arguments to command line"""
        super(SphericalIndexer, cls).add_indexing_args(parser)

        parser.add_argument(
            '--fill-missing', action='store_true',
            help='attempt to fill in missing lat/lon/times (use with caution)'
        )
        parser.add_argument(
            '--gap-threshold', type=float,
            help='distance threshold considered for discontinuities'
                 ' in cartesian units'
        )
        parser.add_argument(
            '--use_clustering', action='store_true',
            help='if the swath data have cut out sections. The tiling'
                 ' will try to detect the discontinuities by'
                 ' clustering.'
        )

    def discrete_geometry(self, lons, lats, **kwargs):
        return Point(lons, lats)

    def polyline_geometry(self, lons, lats, **kwargs):
        return GeoShape.polyline(lons, lats)

    @classmethod
    def set_segment_geometry(
            cls,
            segment: FeatureIndexRecord,
            feature: Feature,
            mask: np.ndarray = None,
            **kwargs):
        """set the geometry of a FeatureIndexRecord object from its segments

        Args:
            mask: mask to apply to the data to fit the shape to valid data
                only in the segment. The mask must fit lat and lon dimensions.
            precision (int): geohash precision used to retrieve the shape of the
                data contained in the tile.
        """
        if mask is not None:
            mask = mask[segment.slices]

        geometry = GeoShape.polygon(
            feature.get_lon(index=segment.slices),
            feature.get_lat(index=segment.slices),
            **kwargs)
        if geometry is None:
            logging.debug(f'Skipped an invalid segment : {segment.slices}')
            return

        segment.geometry = geometry

    def bounding_geometry(
            self,
            segments: T.List[SegmentIndexRecord],
            feature_type: str,
            simplify_factor: float = 0.1,
            **kwargs) -> shapely.geometry.base:
        """return the bounding geometry of a FeatureIndexRecord object from
        the jointure of its segments coverages.

        Args:
            segments: the list of segments part of the feature
            feature_type: the type of feature as defined in cerbere
            simplify_factor: simplify (as in shapely) factor applied to the
                joint geometry
        """
        if feature_type.lower() == 'omdcollection':
            if len(segments) == 1:
                geometry = segments[0].geometry
            else:
                raise NotImplementedError

        elif feature_type.lower() == 'trajectory':
            lines = []
            for seg in segments:
                # build a linestring
                # @TODO simplifier ?
                if isinstance(seg.geometry, LineString):
                    lines.append(seg.geometry)
                elif isinstance(seg.geometry, MultiLineString):
                    lines.extend(seg.geometry.geoms)
            geometry = shapely.ops.linemerge(lines)
        else:
            geometry = shapely.ops.unary_union([_.geometry for _ in segments])

        # deals with some left holes when unifying polygons
        if feature_type.lower() not in ['omdcollection', 'trajectory']:
            eps = 0.1
            geometry = (
                geometry
                .buffer(eps, 1, join_style=shapely.geometry.JOIN_STYLE.mitre)
                .buffer(-eps, 1, join_style=shapely.geometry.JOIN_STYLE.mitre)
                .intersection(WORLD)
            )

        return geometry.simplify(simplify_factor, preserve_topology=True)

    @classmethod
    def clean_polygon(cls, geometry):
        # deals with some left holes when unifying polygons
        eps = 0.1
        return geometry \
            .buffer(eps, 1, join_style=shapely.geometry.JOIN_STYLE.mitre) \
            .buffer(-eps, 1, join_style=shapely.geometry.JOIN_STYLE.mitre) \
            .intersection(WORLD)
