'''
Created on 8 aout 2015

@author: sumba
'''
import numpy


def get_angle(p0, p1=numpy.array([0,0]), p2=None):
    ''' compute angle (in degrees) for p0p1p2 corner
    
    Args:
        p0,p1,p2 - points in the form of [x,y]
    '''
    if p2 is None:
        p2 = p1 + numpy.array([1, 0])
    v0 = numpy.array(p0) - numpy.array(p1)
    v1 = numpy.array(p2) - numpy.array(p1)

    angle = numpy.math.atan2(numpy.linalg.det([v0,v1]),numpy.dot(v0,v1))
    return numpy.degrees(angle)