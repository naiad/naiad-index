"""
.. module::naiad.ingestion.contribs.s3slstrtiler

Class to extract the tiles from a S3 OLCI product granule

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

from naiad.processing.contribs.s3tiler import S3Tiler

    
class S3OLCITiler(S3Tiler):
    """Class to extract the shape and tiles from a S3 SLSTR product granule.

    This class inherits from the Tiler class which performs the extraction of
    the tiles. It implements the specific tile and granule properties for a
    GHRSST product..
    """
    def process_tile_properties(self, feature, tiles):
        """Process the specific properties for a GHRSST product.
        """
        # get general properties of any S3 product
        super(S3OLCITiler, self).process_tile_properties(feature, tiles)
        
        # now get specific SLSTR properties
        
        # put data in cache
        feature.get_values("WQSF")

        # extract properties, based on WQSF flag
        for tile in tiles:
            
            # coastline


            # ocean
            data = feature.get_field("WQSF").bitmask_or(['WATER'], slices=tile.slice())
            val = numpy.count_nonzero(data) / float(data.size) * 100.
            tile.properties['ocean_percentage'] = round(val, 2)

            # land
            data = feature.get_field("WQSF").bitmask_or(['LAND'], slices=tile.slice())
            val = numpy.count_nonzero(data) / float(data.size) * 100.
            tile.properties['land_percentage'] = round(val, 2)

            # inland_water
            data = feature.get_field("WQSF").bitmask_or(['INLAND_WATER'], slices=tile.slice())
            val = numpy.count_nonzero(data) / float(data.size) * 100.
            tile.properties['inland_water_percentage'] = round(val, 2)

            # sun_glint
            attributes = feature.get_field("WQSF").attributes
            if 'HIGHGLINT' not in attributes['flag_meanings']:
                data = feature.get_field("WQSF").bitmask_or(['MEGLINT'], slices=tile.slice())
            else:
                data = feature.get_field("WQSF").bitmask_or(['MEGLINT', 'HIGHGLINT'], slices=tile.slice())
            val = numpy.count_nonzero(data) / float(data.size) * 100.
            tile.properties['sunglint_percentage'] = round(val, 2)


    def process_granule_properties(self, feature, record):
        """Add the additional properties to store in the product index for a
        granule.
        """
        _, selected_properties = self.get_properties(feature.get_url())
        for prop, value in selected_properties.iteritems():
            record.properties[prop] = value

    def run(self, pattern, mapperclass,
            steps,
            orientation=None,
            outpath='.',
            display=False,
            featureclass=None,
            doubleswath=None,
            view=None,
            archive=False,
            **kwargs):
        """Performs the tiling of input files. Actually uses the manifest file
        and don't do any tiling at all: the whole file is a tile.
        """
        super(S3Tiler, self).run(pattern, mapperclass, steps, orientation, outpath, display, featureclass, doubleswath, view, archive)