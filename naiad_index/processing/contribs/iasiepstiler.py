"""
.. module::naiad.ingestion.contribs.iasiepstiler

Class to extract the tiles from a EUMETSAT IASI EPS L1 product granule

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from naiad.processing.tiler import Tiler

from cerform import zenital


class IASIEPSTiler(Tiler):
    """Class to extract the shape and tiles from a EUMETSAT IASI EPS L1 product
    granule.

    This class inherits from the Tiler class which performs the extraction of
    the tiles. It implements the specific tile and granule properties for a
    EUMETSAT IASI EPS L1 product.
    """
    def process_tile_properties(self, feature, tiles):
        """Process the specific properties for a EUMETSAT IASI EPS L1 product.
        """
        # compute zenital solar angle field
        zen = zenital.get_sun_zenital_angle(feature)
        # extract properties
        for tile in tiles:
            min_solzen = zen.get_values(tile.slice()).min()
            max_solzen = zen.get_values(tile.slice()).max()
            tile.properties['solar_zenith_angle_min'] = round(min_solzen, 1)
            tile.properties['solar_zenith_angle_max'] = round(max_solzen, 1)

    def process_granule_properties(self, feature, record):
        """Add the additional properties to store in the product index for a
        granule.
        """
        # compute zenital solar angle field
        zen = zenital.get_sun_zenital_angle(feature)
        min_solzen = zen.get_values().min()
        max_solzen = zen.get_values().max()
        record.properties['solar_zenith_angle_min'] = round(min_solzen, 1)
        record.properties['solar_zenith_angle_max'] = round(max_solzen, 1)
        return
