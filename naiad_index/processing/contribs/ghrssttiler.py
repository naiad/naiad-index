"""
.. module::naiad.ingestion.contribs.ghrssttiler

Class to extract the tiles from a GHRSST product granule

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

from naiad.processing.tiler import Tiler
from cerform import zenital


class GHRSSTTiler(Tiler):
    """Class to extract the shape and tiles from a GHRSST product granule.

    This class inherits from the Tiler class which performs the extraction of
    the tiles. It implements the specific tile and granule properties for a
    GHRSST product..
    """
    def process_tile_properties(self, feature, tiles):
        """Process the specific properties for a GHRSST product.
        """
        # compute zenital solar angle field
        zen = zenital.get_sun_zenital_angle(feature)
        
        # get land and ice bit masks
        
                           
        # extract properties
        for tile in tiles:
            mean_solzen = zen.get_values(tile.slice()).mean()
            tile.properties['solzen'] = round(mean_solzen, 1)
            
            quality = feature.get_values("quality_level", tile.slice())
            qual1 = numpy.count_nonzero(quality == 1)
            qual1to5 = numpy.count_nonzero(quality >= 1)
            if qual1to5 == 0:
                tile.properties['cloud_coverage'] = 0.
            else:
                tile.properties['cloud_coverage'] = (qual1 * 100. / qual1to5)

            flagland = feature.get_field("l2p_flags").bitmask_or(
                "land", tile.slice())
            tile.properties['land_coverage'] = (
                numpy.count_nonzero(flagland) * 100. / flagland.size)
            flagice = feature.get_field("l2p_flags").bitmask_or(
                "ice", tile.slice())
            tile.properties['ice_coverage'] = (
                numpy.count_nonzero(flagice) * 100. / flagice.size)

    def process_granule_properties(self, feature, record):
        """Add the additional properties to store in the product index for a
        granule.
        """
        quality = feature.get_metadata()['file_quality_level']
        record.properties['file_quality_level'] = quality
        
        quality = self.cache['quality_level']
        qual1 = numpy.count_nonzero(quality == 1)
        qual1to5 = numpy.count_nonzero(quality >= 1)
        if qual1to5 == 0:
            record.properties['cloud_coverage'] = 0.
        else:
            record.properties['cloud_coverage'] = (qual1 * 100. / qual1to5)

        flagland = feature.get_field("l2p_flags").bitmask_or("land")
        record.properties['land_coverage'] = (
            numpy.count_nonzero(flagland) * 100. / flagland.size)
        flagice = feature.get_field("l2p_flags").bitmask_or("ice")
        record.properties['ice_coverage'] = (
            numpy.count_nonzero(flagice) * 100. / flagice.size)

        return

    def loadcache(self, feature):
        """Read the required variables content and put it in cache.
        
        To be overriden by a inherited tiler to add the additional required
        variables to be read
        """
        super(GHRSSTTiler, self).loadcache(feature)
        self.cache['quality_level'] = feature.get_values("quality_level", cache=True)
        self.cache['l2p_flags'] = feature.get_values("l2p_flags", cache=True)

