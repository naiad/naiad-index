"""
.. module::naiad.ingestion.contribs.s3slstrtiler

Class to extract the tiles from a S3 SLSTR product granule

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

from naiad.processing.contribs.s3tiler import S3Tiler
from cerform import zenital


class S3SLSTRTiler(S3Tiler):
    """Class to extract the shape and tiles from a S3 SLSTR product granule.

    This class inherits from the Tiler class which performs the extraction of
    the tiles. It implements the specific tile and granule properties for a
    GHRSST product..
    """
    def loadcache(self, feature):
        """Read the required variables content and put it in cache.
        
        To be overriden by a inherited tiler to add the additional required
        variables to be read
        """
        super(S3SLSTRTiler, self).loadcache(feature)
        if 'satellite_zenith_angle' in feature.get_fieldnames():
            self.cache['satellite_zenith_angle'] = feature.get_values(
                "satellite_zenith_angle", cache=True)       
        
        if 'cloud_in' in feature.get_fieldnames():
            # WCT and RBT only
            self.cache['confidence_in'] = feature.get_values(
                "confidence_in", cache=True)
            self.cache['cloud_in'] = feature.get_values(
                "cloud_in", cache=True)
            self.cache['solar_zenith_tn'] = feature.get_values(
                "solar_zenith_tn", cache=True)
            self.cache['satellite_zenith_angle'] = feature.get_values(
                "sat_zenith_tn", cache=True)
                
    def process_tile_properties(self, feature, tiles):
        """Process the specific properties for a GHRSST product.
        """
        # get general properties of any S3 product
        super(S3SLSTRTiler, self).process_tile_properties(feature, tiles)

        # now get specific SLSTR properties
        for tile in tiles:
            if 'confidence_in' in feature.get_fieldnames():
                data = feature.get_values("confidence_in", slices=tile.slice())

                # coastline
                data = feature.get_field("confidence_in").bitmask_or(
                    'coastline',
                    slices=tile.slice()
                )
                val = numpy.count_nonzero(data) / float(data.size) * 100.
                tile.properties['coastline_percentage'] = round(val, 2)

                # ocean
                data = feature.get_field("confidence_in").bitmask_or(
                    'ocean',
                    slices=tile.slice()
                )
                val = numpy.count_nonzero(data) / float(data.size) * 100.
                tile.properties['ocean_percentage'] = round(val, 2)

                # land
                data = feature.get_field("confidence_in").bitmask_or(
                    'land',
                    slices=tile.slice()
                )
                val = numpy.count_nonzero(data) / float(data.size) * 100.
                tile.properties['land_percentage'] = round(val, 2)

                # inland_water
                data = feature.get_field("confidence_in").bitmask_or(
                    'inland_water',
                    slices=tile.slice()
                )
                val = numpy.count_nonzero(data) / float(data.size) * 100.
                tile.properties['inland_water_percentage'] = round(val, 2)

                # sun_glint
                data = feature.get_field("confidence_in").bitmask_or(
                    'sun_glint',
                    slices=tile.slice()
                )
                val = numpy.count_nonzero(data) / float(data.size) * 100.
                tile.properties['sunglint_percentage'] = round(val, 2)

                if "sat_zenith_tn" in feature.get_fieldnames():
                    data = feature.get_values("sat_zenith_tn", slices=tile.slice())
                else:
                    data = feature.get_values("satellite_zenith_angle", slices=tile.slice())
                if data.any():
                    value = data.min()
                    tile.properties['sat_zenith_angle_min'] = round(value, 2)
                    value = data.max()
                    tile.properties['sat_zenith_angle_max'] = round(value, 2)

            # cloud coverage
            if 'cloud_in' in feature.get_fieldnames():
                mask = ['1.6_small_histogram', '1.6_large_histogram',
                        '2.25_small_histogram', '2.25_large_histogram',
                        'gross_cloud', 'thin_cirrus', 'medium_high',
                        '11_12_view_difference', '3.7_11_view_difference',
                        'fog_low_stratus', 'thermal_histogram']

                cloudy = feature.get_field('cloud_in').bitmask_or(
                    mask, slices=tile.slice())
                water = feature.get_field("confidence_in").bitmask_or(
                    ["ocean", "inland_water"], slices=tile.slice())

                if float(numpy.count_nonzero(water)) != 0:
                    value = numpy.count_nonzero(
                        cloudy & water) / float(numpy.count_nonzero(water)) * 100.
                    tile.properties['cloud_coverage'] = round(value, 2)

    def process_granule_properties(self, feature, record):
        """Add the additional properties to store in the product index for a
        granule.
        """
        properties = [
            'absolute_orbit_number',
            'relative_orbit_number',
            'phase',
            'cycle',
            'ground_track_direction',
            'processing_l1_facility_name',
            'processing_l1_facility_name',
            'processing_l1_facility_hardware',
            'processing_l1_facility_software_version',
            'processing_l1_facility_software_name',
            'processing_l1_start',
            'processing_l1_stop',

            'l1_slstr_class_summary_1km_coastal_pix',
            'l1_slstr_class_summary_1km_land_pix',
            'l1_slstr_class_summary_1km_tidal_pix',
            'l1_slstr_class_summary_1km_cloudy_region_pix',
            'l1_slstr_class_summary_1km_fresh_inland_water_pix',

            'l2_slstr_class_summary_1km_coastal_pix',
            'l2_slstr_class_summary_1km_land_pix',
            'l2_slstr_class_summary_1km_tidal_pix',
            'l2_slstr_class_summary_1km_cloudy_region_pix',
            'l2_slstr_class_summary_1km_fresh_inland_water_pix',
        ]
        _, selected_properties = self.get_properties(feature.get_url())
        if _ is not None:
            for prop, value in selected_properties.iteritems():
                # keep only manifest properties selected for indexing
                if prop in properties:
                    # remove ambiguity prefixes
                    if prop.startswith("l1_") or prop.startswith("l2_"):
                        prop = prop[3:]
                    
                    # catch nan values and transform to None
                    try:
                        if numpy.isnan(value):
                            value = None
                    except:
                        pass
    
                    record.properties[prop] = value

        if "solar_zenith_tn" in feature.get_fieldnames():
            data = feature.get_values("solar_zenith_tn")

        else:
            # calculate sun zenith angle
            data = zenital.get_sun_zenital_angle(feature).get_values()
        if data.any():
            record.properties['solar_zenith_angle_min'] = round(data.min(), 2)
            record.properties['solar_zenith_angle_max'] = round(data.max(), 2)

#         # cloud coverage
#         if 'cloud_in' in feature.get_fieldnames():
#             mask = ['1.6_small_histogram', '1.6_large_histogram',
#                     '2.25_small_histogram', '2.25_large_histogram',
#                     'gross_cloud', 'thin_cirrus', 'medium_high',
#                     'fog_low_stratus', 'thermal_histogram']
#             cloudy = feature.get_field('cloud_in').bitmask_or(mask)
#             ocean = feature.get_field("confidence_in").bitmask_or(
#                 ["ocean", "inland_water"])
#             record.properties['cloud_coverage'] = numpy.count_nonzero(
#                 cloudy & ocean) / float(numpy.count_nonzero(ocean)) * 100.

    def run(self, pattern, mapperclass,
            steps,
            orientation=None,
            outpath='.',
            display=False,
            featureclass=None,
            doubleswath=None,
            view=None,
            archive=False,
            **kwargs):
        """Performs the tiling of input files.
        """
        super(S3Tiler, self).run(pattern, mapperclass, steps, orientation,
                                 outpath, display, featureclass, doubleswath, view, archive)
