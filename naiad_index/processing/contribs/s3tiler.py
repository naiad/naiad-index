"""
.. module::naiad.ingestion.contribs.ghrssttiler

Class to extract the tiles from a GHRSST product granule

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import glob
import os
import logging
import copy
import dateutil.parser as parser
from collections import OrderedDict

from shapely.geometry import Polygon
from shapely.geometry.polygon import orient
import matplotlib.pyplot as plt

from naiad.processing.tiler import Tiler
from naiad.processing.tile import Tile
from naiad.processing.geoshape import GeoShape
import cerbere.utils.safes3.manifest as manifest

from cerform import zenital


class S3Tiler(Tiler):
    """Class to extract the shape and tiles from a GHRSST product granule.

    This class inherits from the Tiler class which performs the extraction of
    the tiles. It implements the specific tile and granule properties for a
    GHRSST product..
    """

    def get_properties(self, fname):
        """return the properties from manifest"""
        EXCLUDE_KEYS = ['footPrint_posList',
                        'acquisition_startTime',
                        'acquisition_stopTime',
                        'platform_instrument_familyName',
                        'platform_familyName',
                        'platform_number',
                        'platform_instrument_abbreviation',
                        's3_safe',
                        'product_name',
                        ]

        OLCI_EXCLUDE_KEYS = [
            'olci_rows',
            'olci_columns'
             ] + EXCLUDE_KEYS

        SLSTR_EXCLUDE_KEYS = [
            'slstr_class_1km_oblique_rows',
            'slstr_class_1km_oblique_columns'
        ] + EXCLUDE_KEYS

        SRAL_EXCLUDE_KEYS = [] + EXCLUDE_KEYS

        try:
            self.manifest, _ = manifest.parse(fname)
        except:
            # return empty dict of properties when no manifest found
            logging.warning("Missing manifest file. Some granule properties "
                            "will be missing.")
            return None, {}

        sensor = self.manifest['platform_instrument_abbreviation']['value']
        keys = []
        if sensor == 'OLCI':
            keys = filter(
                lambda x: x not in OLCI_EXCLUDE_KEYS and
                'slstr' not in x and
                'sral' not in x,
                self.manifest.keys()
                )
            rows = int(self.manifest['olci_rows']['value'])
            columns = int(self.manifest['olci_columns']['value'])

        elif sensor == 'SLSTR':
            keys = filter(
                lambda x: x not in SLSTR_EXCLUDE_KEYS and
                'olci' not in x and
                'sral' not in x,
                self.manifest.keys()
            )
            if "_RBT_" in fname:
                rows = int(
                    self.manifest['slstr_class_1km_oblique_rows']['value'])
                columns = int(
                    self.manifest['slstr_class_1km_oblique_columns']['value'])
            else:
                # there is a bug in manifests, for now we put a hardcoded value
                rows = 1200
                columns = 1500

        elif sensor == 'SRAL':
            keys = filter(
                lambda x: x not in SRAL_EXCLUDE_KEYS and
                'slstr' not in x and
                'olci' not in x,
                self.manifest.keys()
            )
        selection = dict(
            map(lambda key: (key, self.manifest.get(key, None)['value']),
                keys)
        )
        self.manifest['rows'] = rows
        self.manifest['columns'] = columns
        return self.manifest, selection

    def process_tile_properties(self, feature, tiles):
        """Process the specific properties for a GHRSST product.
        """
        # compute zenital solar angle field
        zen = zenital.get_sun_zenital_angle(feature)

        # extract properties
        for tile in tiles:
            min_solzen = zen.get_values(slices=tile.slice()).min()
            max_solzen = zen.get_values(slices=tile.slice()).max()
            tile.properties['solar_zenith_angle_min'] = round(min_solzen, 1)
            tile.properties['solar_zenith_angle_max'] = round(max_solzen, 1)

    def run(self, pattern, mapperclass,
            steps,
            orientation=None,
            outpath='.',
            display=False,
            featureclass=None,
            doubleswath=None,
            view=None,
            archive=False,
            **kwargs):
        """Performs the tiling of input files. Actually uses the manifest file
        and don't do any tiling at all: the whole file is a tile.
        """
        files = glob.glob(pattern)
        if len(files) == 0:
            logging.warning("No files were found with this pattern: %s",
                            pattern)
        files.sort()
        for i, fname in enumerate(files):
            logging.info("Processing #{} : {}".format(str(i), fname))

            # get properties from manifest
            properties, selected_properties = self.get_properties(fname)

            if archive:
                # archive in year/day subfolder structure
                fulloutpath = outfname = os.path.join(
                    outpath,
                    properties['acquisition_startTime'].strftime("%Y/%j")
                )
                if not os.path.exists(fulloutpath):
                    os.makedirs(fulloutpath)
            else:
                fulloutpath = outpath
            outfname = os.path.join(fulloutpath,
                                    os.path.basename(fname) + ".tiles")

            # get footprint and set orientation for current granule,
            footprint = properties['footPrint_posList']['value']
            shape = orient(Polygon(zip(footprint[0], footprint[1])))
            self.orientation = orient(shape)

            logging.debug("Bounding shape : {} with orientation {}",
                          shape, orientation)

            # get granule global tile
            granule = Tile(
                granule=properties['product_name']['value'],
                geometry=shape,
                start=parser.parse(
                    properties['acquisition_startTime']['value']),
                end=parser.parse(properties['acquisition_stopTime']['value']),
                feature='Swath',
                xmin=None, xmax=None, ymin=None, ymax=None,
                properties=selected_properties, product=None)

            # get tiles : only one tile
            logging.warning(
                "only one tile can be extracted from a manifest file")
            tiles = [Tile(
                granule=properties['product_name']['value'],
                geometry=shape,
                start=parser.parse(
                    properties['acquisition_startTime']['value']),
                end=parser.parse(properties['acquisition_stopTime']['value']),
                feature='Swath',
                xmin=0,
                xmax=properties['columns'] - 1,
                ymin=0,
                ymax=properties['rows'] - 1,
                properties=selected_properties, product=None)
            ]

            with open(outfname, 'w') as outfile:
                granule.write(outfile)
                for tile in tiles:
                    tile.write(outfile)

            if display:
                fig = GeoShape.draw(granule.shape, colour="red")
                for tile in tiles:
                    GeoShape.draw(tile.shape, colour="green", figure=fig)
                plt.show()

            outfile.close()
