"""
A list of functions to support geohash based indexing and geometries

Note on precision: use the precision (geohash length) immediately greater than
the resolution of the data (precision 5 for 1km resolution data, 4 for 20 km
data,...)

Geohash length 	Cell width 	Cell height
1 	≤ 5,000km 	× 	5,000km
2 	≤ 1,250km 	× 	625km
3 	≤ 156km 	× 	156km
4 	≤ 39.1km 	× 	19.5km
5 	≤ 4.89km 	× 	4.89km
6 	≤ 1.22km 	× 	0.61km
7 	≤ 153m 	× 	153m
8 	≤ 38.2m 	× 	19.1m
9 	≤ 4.77m 	× 	4.77m
10 	≤ 1.19m 	× 	0.596m
11 	≤ 149mm 	× 	149mm
12 	≤ 37.2mm 	× 	18.6mm


"""
import itertools
import logging
import typing as T

import geopandas as gpd
from polygon_geohasher.polygon_geohasher import \
    polygon_to_geohashes, geohashes_to_polygon
import matplotlib.pyplot as plt
import numpy as np
import shapely.geometry

from cerbere.feature.feature import Feature

from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord
from .segmentindexer import SegmentIndexer, SegmentIndexerSettings


# number of level+1 geohashes for a geohash of a given precision level
GH_COUNT = 32


# specific arguments
class GeohashIndexerSettings(SegmentIndexerSettings):
    precision: int = 4


def latlon_to_geohash(
        lat, lon, precision=4, mask: 'np.ndarray' = None) -> T.List[str]:
    """Returns the list of geohashes for a list of positions"""
    try:
        import pyinterp.geohash as geohash
    except ModuleNotFoundError:
        logging.error(
            'geohash package is not installed. Optional geohash based tiling '
            'is not available. Install geohash package.')
        raise

    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)

    clat = np.ma.masked_where(lat >= 90., lat).compressed().tolist()
    clon = np.ma.masked_where(lat >= 90., lon).compressed().tolist()
    geo = np.unique(geohash.encode(clon, clat, precision=precision))

    return geo.astype(np.unicode_)


def to_geohash(feature,
               precision: int = 4,
               mask: 'np.ndarray' = None,
               **kwargs) -> T.List[str]:
    """Returns the list of geohashes for a cerbere feature"""
    lat = feature.get_lat(expand=True)
    lon = feature.get_lon(expand=True)
    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)
    return latlon_to_geohash(lat, lon, precision)


def latlon_to_footprint(lat, lon, precision: int = 4, **kwargs) -> T.List[str]:
    """Returns the footprint polygon matching a list of positions, based on
    geohash extraction method.

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.
    """
    geohashes = latlon_to_geohash(lat, lon, precision)
    reduced_geohashes = reduce(geohashes, **kwargs)

    return to_polygon(reduced_geohashes, **kwargs)


def footprint(feature,
              precision: int = 4,
              mask: 'np.ndarray' = None,
              **kwargs) -> T.List[str]:
    """Returns the footprint polygon matching a cerbere feature, based on
    geohash extraction method

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.

    Args:
        mask: mask of invalid measurements (to remove these locations from the
            footprint computation)
    """
    lat = feature.get_lat(expand=True)
    lon = feature.get_lon(expand=True)
    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)
    return latlon_to_footprint(lat, lon, precision=precision, **kwargs), []


def reduce(geohashes: T.List[str], min_level: int = 2, **kwargs) -> T.List[str]:
    """
    Reduce a list of geohashes so that level+1 geohashes are
    replaced with a level geohash whenever possible.

    Args:
        geohashes: list of geohashes of identical level
        min_level: minimum precision to which geohashes are reduced

    Return:
        a reduced list of geohashes of different precision
    """
    if len(geohashes) == 0:
        return geohashes

    geohashes.sort()

    reduced = []
    remainders = []
    k = 0
    while True:
        if k == len(geohashes):
            break
        if k + GH_COUNT >= len(geohashes):
            remainders.extend(geohashes[k:])
            break

        next_geo = set(
            [_[:-1] for _ in geohashes[k:min(k + GH_COUNT, len(geohashes))]])
        if len(next_geo) == 1:
            reduced.append(geohashes[k][:-1])
            k += GH_COUNT
        else:

            remainders.append(geohashes[k])
            k += 1

    if len(geohashes[0]) > min_level and len(reduced) > 0:
        reduced = reduce(reduced)

    reduced.extend(remainders)
    return reduced


def to_polygon(geohashes: T.List[str], concave=False, **kwargs):
    """
    Returns the polygon matching a set of geohashes.

    Contiguous geohashes are considered as belonging to the same polygon. Apply
    only on a reduced list of geohashes as it can be extremely inefficient on a
    large number of geohashes.

    Args:
        geohashes: list of geohashes to reconstruct polygons from
        concave: simplify by taking the concave hull of the returned polygon to
            avoid aliasing effect. Uses with caution.
    """
    polygon = geohashes_to_polygon(geohashes)

    # simplify by taking the convex hull (does not work very well in some
    # situations like polar areas)

    # if concave:
    #     # @TODO: alpha must be an argument
    #     import alphashape
    #     if isinstance(polygon, geometry.MultiPolygon):
    #         return ops.cascaded_union([
    #             alphashape.alphashape(_.exterior.coords, 4) for _ in polygon])
    #     else:
    #         return alphashape.alphashape(polygon.exterior.coords, 4)

    # if convex:
    #     if isinstance(polygon, geometry.MultiPolygon):
    #         return ops.cascaded_union([_.convex_hull for _ in polygon])
    #     else:
    #         return polygon.convex_hull

    return polygon


def plot(feature=None, positions=None, geohashes=None, polygon=None, **kwargs):
    try:
        import pyinterp
    except ModuleNotFoundError:
        logging.error(
            'geohash package is not installed. Optional geohash based tiling '
            'is not available. Install geohash package.')
        raise
    fig, axs = plt.subplots(figsize=(50, 50), **kwargs)

    if geohashes is not None:
        import shapely
        p = gpd.GeoSeries(
            [shapely.wkt.loads(
                str(pyinterp.GeoHash.from_string(_).bounding_box().wkt()))
                for _ in geohashes])
        gpd.GeoSeries.plot(p, ax=axs, aspect='auto', color='g', edgecolor='k')

    # if positions is not None:
    #     lat, lon = positions
    #     plt.scatter(lon, lat, alpha=0.6, s=0.01)
    #
    # if feature is not None:
    #     plt.scatter(feature.get_lon(), feature.get_lat(), alpha=0.3, s=0.01)

    # if polygon is not None:
    #     p = gpd.GeoSeries(polygon)
    #     gpd.GeoSeries.plot(
    #         p, ax=axs, aspect='auto', facecolor="none", edgecolor='r')

    plt.show()


class GeohashIndexer(SegmentIndexer):

    identifier: str = 'geohash'
    help: str = 'use geohash based spatial information extraction'
    setting_class = GeohashIndexerSettings

    def __init__(self):
        # cache for the geohash of each feature measurements
        self._geohashes = None

    @classmethod
    def add_indexing_args(cls, parser):
        """Adding specific arguments to command line"""
        super(GeohashIndexer, cls).add_indexing_args(parser)

        parser.add_argument(
            '--precision', type=int, default=4,
            help='geohash precision : use the precision (geohash length) '
                 'immediately greater than the resolution of the data (ex: '
                 'precision 5 for 1km resolution data, 4 for 20 km) (default=4)'
        )

    def geohashes_from_latlon(
            self,
            lats: np.ndarray,
            lons: np.ndarray,
            geohashes: np.ndarray = None,
            precision: int = 4,
            preserve_continuity: bool = True
    ):
        """
        Args:
            preserve continuity: if True, adjacent measurements should have a
                continuous list of geohashes joining them. Measurements are
                considered as surfaces in this case and there should
                therefore be no gaps between them.
        """
        if geohashes is None:
            geohashes = latlon_to_geohash(lats, lons, precision=precision)

        # degrade precision in high lats to simplify the shape
        # @TODO not good....
        highlats = np.ma.fabs(lats) > 86.
        if highlats.sum() > 0:
            geohashes = np.concatenate([
                geohashes,
                latlon_to_geohash(
                    lats[highlats],
                    lons[highlats],
                    precision=max(precision - 1, 1))
            ])

        # reduce the number of geohashes by grouping them
        reduced_geohashes = reduce(geohashes)

        return list(set(reduced_geohashes))

    def set_segment_geometry(
            self,
            segment: FeatureIndexRecord,
            feature: Feature,
            mask: np.ndarray = None,
            precision: int = 4,
            **kwargs):
        """set the geometry of a FeatureIndexRecord object from its segments

        Args:
            mask: mask to apply to the data to fit the shape to valid data
                only in the segment. The mask must fit lat and lon dimensions.
            precision (int): geohash precision used to retrieve the shape of the
                data contained in the tile.
        """
        lats = feature.get_lat(index=segment.slices, expand=True)
        lons = feature.get_lon(index=segment.slices, expand=True)
        if mask is not None:
            mask = mask[segment.slices]
            lats = np.ma.masked_where(mask, lats)
            lons = np.ma.masked_where(mask, lons)

        reduced_geohashes = self.geohashes_from_latlon(
            lats, lons, precision=precision)

        segment.geometry = to_polygon(reduced_geohashes, **kwargs)
        segment.properties['geohashes'] = reduced_geohashes

    def bounding_geometry(
            self,
            segments: T.List[SegmentIndexRecord],
            feature_type: str,
            simplify_factor: float = 0.1,
            **kwargs) -> shapely.geometry.base:
        """return the bounding geometry of a FeatureIndexRecord object from
        the jointure of its segments coverages.

        Args:
            segments: the list of segments part of the feature
            feature_type: the type of feature as defined in cerbere
            simplify_factor: simplify (as in shapely) factor applied to the
                joint geometry
        """
        geohashes = itertools.chain.from_iterable(
                [seg.properties['geohashes'] for seg in segments]
            )
        reduced_geohashes = reduce(list(set(geohashes)))
        return to_polygon(reduced_geohashes)

    def _geoindexrecord_from_feature(
            self,
            feature: Feature,
            settings: GeohashIndexerSettings,
            skip_feature_geometry: bool = False
    ) -> FeatureIndexRecord:

        georecord = super(GeohashIndexer, self)._geoindexrecord_from_feature(
            feature, settings, skip_feature_geometry=skip_feature_geometry)

        if True and georecord.segments is not None:
            # for debug
            geohashes = itertools.chain.from_iterable(
                [seg.properties['geohashes'] for seg in georecord.segments]
            )
            reduced_geohashes = reduce(list(set(geohashes)))
            plot(feature=None,
                 positions=(feature.get_lat(),
                            feature.get_lon()),
                 geohashes=reduced_geohashes,
                 polygon=georecord.geometry)

        # remove geohashes from segments temporarily memorized
        if georecord.segments is not None:
            for t in georecord.segments:
                if 'geohashes' in t.properties:
                    t.properties.pop('geohashes')

        return georecord
