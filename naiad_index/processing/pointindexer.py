"""
An indexing method that considers all data points as individual surfaces and
calculate the footprints by merging all these surfaces all together.

the size of the surfaces is defined by a diameter (resolution)
"""
import typing as T

import numpy as np
import shapely.geometry

from cerbere.feature.feature import Feature

from naiad_commons.data.geoindexrecord import FeatureIndexRecord, \
    SegmentIndexRecord
from .segmentindexer import SegmentIndexer, SegmentIndexerSettings


# specific arguments
class PointIndexerSettings(SegmentIndexerSettings):
    pass


class PointIndexer(SegmentIndexer):

    identifier: str = 'point'
    help: str = 'use point based spatial information extraction'
    setting_class = PointIndexerSettings

    def __init__(self):
        # cache for the geohash of each feature measurements
        self._geohashes = None

    @classmethod
    def add_indexing_args(cls, parser):
        """Adding specific arguments to command line"""
        super(PointIndexer, cls).add_indexing_args(parser)

    def polygon_from_latlon(
            self,
            lat: np.ndarray,
            lon: np.ndarray,
            resolution: float):
        """Return a polygon shapely geometry from a list of lat,lon.

        Each point is representing a circular surface which diameter is
        defined by the resolution, in degrees
        """
        R = 6371

        # adjust surface radius wrt to latitude
        r = np.degrees(resolution * np.sqrt(2.) / (R * np.cos(np.radians(lat))))

        # union of each data point surface
        poly = shapely.ops.unary_union(
            [shapely.geometry.Point(lon[i], lat[i]).buffer(r[i])
             for i, _ in enumerate(lat)])

        return poly

    def set_segment_geometry(
            self,
            segment: FeatureIndexRecord,
            feature: Feature,
            mask: np.ndarray = None,
            resolution: float = None,
            **kwargs):
        """set the geometry of a FeatureIndexRecord object from its segments

        Args:
            mask: mask to apply to the data to fit the shape to valid data
                only in the segment. The mask must fit lat and lon dimensions.
            precision (int): geohash precision used to retrieve the shape of the
                data contained in the tile.
        """
        lats = feature.get_lat(index=segment.slices, expand=True)
        lons = feature.get_lon(index=segment.slices, expand=True)
        if mask is not None:
            mask = mask[segment.slices]
            lats = np.ma.masked_where(mask, lats)
            lons = np.ma.masked_where(mask, lons)

        segment.geometry = self.polygon_from_latlon(
            lats.compressed(), lons.compressed(), resolution=resolution)

    def bounding_geometry(
            self,
            segments: T.List[SegmentIndexRecord],
            feature_type: str,
            simplify_factor: float = 0.1,
            **kwargs) -> shapely.geometry.base:
        """return the bounding geometry of a FeatureIndexRecord object from
        the jointure of its segments coverages.

        Args:
            segments: the list of segments part of the feature
            feature_type: the type of feature as defined in cerbere
            simplify_factor: simplify (as in shapely) factor applied to the
                joint geometry
        """
        geometry = shapely.ops.unary_union(
            [seg.geometry for seg in segments])

        return geometry
