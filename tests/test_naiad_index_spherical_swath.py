from pathlib import Path
import pytest

from naiad_index.cli.indexing import parse_args, main


TEST_FILE = Path(
    'data/avhrr_metop/20200409035203-OSISAF-L2P_GHRSST-SSTsubskin'
    '-AVHRR_SST_METOP_B-sstmgr_metop01_20200409_035203-v02.0-fv01.0.nc')
TEST_FILE = Path('data/avhrr_metop/20200409035203-OSISAF-L2P_GHRSST-SSTsubskin'
                 '-AVHRR_SST_METOP_B-sstmgr_metop01_20200409_035203-v02.0'
                 '-fv01.0.nc')
#TEST_FILE = \
#    'data/avhrr_metop/20200413194603-OSISAF-L2P_GHRSST-SSTsubskin
#    -AVHRR_SST_METOP_B-sstmgr_metop01_20200413_194603-v02.0-fv01.0.nc'

WKT = 'POLYGON ((180 -80, 180 80, -180 80, -180 -80, 180 -80))'


def test_command_help():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        search_args = parse_args(['--help'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_command_args():
    index_args, tiler_class = parse_args([
        str(TEST_FILE),
        'spherical',
        '--output-dir', '.',
        '--dataset-class', 'GHRSSTNCDataset',
        '--feature-class', 'Swath'
    ])
    assert(index_args.indexer.footprint_from_attrs is False)


def test_index_spherical(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'GHRSSTNCDataset',
            '--feature-class', 'Swath',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) == 1


def test_index_spherical_with_segments(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'GHRSSTNCDataset',
            '--feature-class', 'Swath',
            '--segment-steps', 'row:512', 'cell:540',
            '--index-segments',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) > 1