from pathlib import Path
import pytest

from naiad_index.cli.indexing import parse_args, main


# TEST_FILE = Path(
#     'tests/data/trajectory'
#     '/S6A_P4_2__HR______20220609T090633_20220609T100246_20220703T005059_3373_058_091_045_EUM__OPE_NT_F06.SEN6')
TEST_FILE = Path(
    'tests/data/trajectory/'
    'S6A_P4_2__LR______20220629T002232_20220629T011845_20230322T160136_3373_060_086_043_EUM__REP_NT_F08.SEN6'
)

def test_command_help():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        search_args = parse_args(['--help'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_command_args():
    index_args, tiler_class = parse_args([
        str(TEST_FILE),
        'spherical',
        '--output-dir', '.',
        '--dataset-class', 'EUMETSATSentinel6STD1Hz',
        '--feature-class', 'Trajectory'
    ])
    assert(index_args.indexer.footprint_from_attrs is False)


def test_index_spherical(tmpdir):
    print(tmpdir)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--show',
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'EUMETSATSentinel6STD1Hz',
            '--feature-class', 'Trajectory'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) == 1


def test_index_spherical_with_segments(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            str(TEST_FILE),
            '--show',
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'EUMETSATSentinel6STD1Hz',
            '--feature-class', 'Trajectory',
            '--segment-steps', 'time:50',
            '--gap-threshold', '1.',
            '--index-segments',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) > 1
