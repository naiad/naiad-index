from pathlib import Path
import pytest

from naiad_index.cli.indexing import parse_args, main


TEST_FILE = Path(
    'data/trajectory/CFO_OP06_SWI_L2ANAD_F_20220705T194558_20220705T211854.nc')


def test_command_help():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        search_args = parse_args(['--help'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_command_args():
    index_args, tiler_class = parse_args([
        str(TEST_FILE),
        'spherical',
        '--output-dir', '.',
        '--dataset-class', 'CWWICSWIML2ANAD1HzNCDataset',
        '--feature-class', 'Trajectory'
    ])
    assert(index_args.indexer.footprint_from_attrs is False)


def test_index_spherical(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--show',
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'CWWICSWIML2ANAD1HzNCDataset',
            '--feature-class', 'Trajectory',
            '--segment-steps', 'time:50',
            '--dilate', '0.7'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) == 1


def test_index_spherical_with_segments(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--show',
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'CWWICSWIML2ANAD1HzNCDataset',
            '--feature-class', 'Trajectory',
            '--segment-steps', 'time:50',
            '--index-segments',
            '--dilate', '0.7'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) > 1
