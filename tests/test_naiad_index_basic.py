import pytest

from naiad_index.cli.indexing import parse_args, main


TEST_FILE = \
    'data/20220607-IFR-L3C_GHRSST-SSTsubskin-ODYSSEA-GLOB_010_adjusted' \
    '-v2.0-fv1.0.nc'
WKT = 'POLYGON ((180 -80, 180 80, -180 80, -180 -80, 180 -80))'


def test_command_help():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        search_args = parse_args(['--help'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_command_args():
    index_args, tiler_class = parse_args([
        TEST_FILE,
        'basic',
        '--footprint-from-attrs',
        '--output-dir', '.',
        '--dataset-class', 'NCDataset',
        '--feature-class', 'CylindricalGrid'
    ])
    assert(index_args.indexer.footprint_from_attrs is True)


def test_index_footprint_from_attrs(tmpdir):

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            TEST_FILE,
            'basic',
            '--footprint-from-attrs',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'NCDataset',
            '--feature-class', 'CylindricalGrid'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_index_footprint_from_attrs_wkt(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            TEST_FILE,
            'basic',
            '--footprint-from-wkt', WKT,
            '--output-dir', str(tmpdir),
            '--dataset-class', 'NCDataset',
            '--feature-class', 'CylindricalGrid'
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
