from pathlib import Path
import pytest

from naiad_index.cli.indexing import parse_args, main


TEST_FILE = Path(
    'data/swath/hscat_20220730_070740_hy_2b__18879_o_250_ovw_l2.nc')


def test_command_help():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        search_args = parse_args(['--help'])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


def test_command_args():
    index_args, tiler_class = parse_args([
        str(TEST_FILE),
        'spherical',
        '--output-dir', '.',
        '--dataset-class', 'NCDataset',
        '--feature-class', 'Swath',
        '--segment-steps', 'row:26', 'cell:20',
        '--use_clustering', '--gap-threshold', '10.',
        '--orientation', 'counterclockwise',
        '--tiling-method', 'corners',  '--fill-missing',
        '--index-segments',
    ])
    assert(index_args.indexer.footprint_from_attrs is False)


def test_index_spherical(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--show',
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'NCDataset',
            '--feature-class', 'Swath',
            '--segment-steps', 'row:26', 'cell:20',
            '--use_clustering', '--gap-threshold', '10.',
            '--orientation', 'counterclockwise',
            '--tiling-method', 'corners', '--fill-missing',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) == 1


def test_index_spherical_with_segments(tmpdir):
    print(tmpdir)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        main([
            '--show',
            str(TEST_FILE),
            'spherical',
            '--output-dir', str(tmpdir),
            '--dataset-class', 'NCDataset',
            '--feature-class', 'Swath',
            '--segment-steps', 'row:26', 'cell:20',
            '--use_clustering', '--gap-threshold', '10.',
            '--orientation', 'counterclockwise',
            '--tiling-method', 'corners', '--fill-missing',
            '--index-segments',
        ])

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    index_file = tmpdir / TEST_FILE.name + '.idx'
    lines = []
    with open(index_file, 'r') as f:
        for line in f:
            lines.append(line)

    assert len(lines) > 1
