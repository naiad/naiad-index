.. _naiad: https://gitlab.ifremer.fr/naiad/naiad

===========
naiad-index
===========

``naiad`` framework is a suite of tools in python for indexing, searching and
colocating Earth Observation (EO) data, in particular satellite observations.

This package is for extracting spatial, temporal and other properties from EO
data files that can be then indexed into a search engine (which is done
for instance through the `naiad`_ package).

Refer to the documentation at:

http://naiad.readthedocs.io


